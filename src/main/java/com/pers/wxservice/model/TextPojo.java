package com.pers.wxservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 23:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextPojo {

    private String content;

}
