package com.pers.wxservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 23:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendTextPojo {

    /**
     * 发送的用户
     */
    private String touser;

    /**
     * 消息类型
     */
    private String msgtype;

    /**
     * 消息内容
     */
    private TextPojo text;
}
