package com.pers.wxservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @Author: 黄建
 * @Date: 2020/4/12 0:11
 */
@ControllerAdvice
@Slf4j
public class MyExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public void exception(RuntimeException e){
      log.info(e.getMessage());
    }

}
