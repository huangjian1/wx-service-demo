package com.pers.wxservice.value;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 22:19
 */
public class WeChatConstant {

    public static final String TOKEN = "testforwxservice";

    public static final String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";

    public static final String POST_SEND_MSG_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}";

    public static final String appId = "";

    public static final String secret = "";
}
