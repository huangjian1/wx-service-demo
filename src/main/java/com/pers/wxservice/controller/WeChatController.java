package com.pers.wxservice.controller;

import com.pers.wxservice.service.WeChatService;
import com.pers.wxservice.utils.SecuritySHA1Utils;
import com.pers.wxservice.utils.SignatureUtil;
import com.pers.wxservice.value.WeChatConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 22:07
 */
@RestController
@RequestMapping("/api/wechat")
public class WeChatController {

    @Autowired
    private WeChatService weChatService;

    /**
     * GET:验证token
     * POST:发送数据
     *
     * @param request  作用域
     * @param response 作用域
     * @return String
     */
    @RequestMapping(value = {"/getProcessRequest"}, method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public String getProcessRequest(@RequestParam(required = false, value = "signature") String signature,
                                    @RequestParam(required = false, value = "timestamp") String timestamp,
                                    @RequestParam(required = false, value = "nonce") String nonce,
                                    @RequestParam(required = false, value = "echostr") String echostr,
                                    HttpServletRequest request, HttpServletResponse response) throws IOException {

        Boolean isGet=request.getMethod().toLowerCase().equals("get");

        if (isGet) {
            String[] arrTmp = {WeChatConstant.TOKEN, timestamp, nonce};
            Arrays.sort(arrTmp);
            String tmpStr = String.join("", arrTmp);
            String sha1 = SecuritySHA1Utils.getSha1(tmpStr);

            PrintWriter out = null;

            try {
                out = response.getWriter();
                // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，否则接入失败
                if (SignatureUtil.checkSignature(signature, sha1)) {
                    out.print(echostr);
                    out.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                out.close();
            }
        } else {
            weChatService.sendMessage(request);
        }
        return null;
    }
}
