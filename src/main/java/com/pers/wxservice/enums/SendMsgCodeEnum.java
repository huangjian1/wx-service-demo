package com.pers.wxservice.enums;

/**
 * @Author: 黄建
 * @Date: 2020/4/12 0:06
 */

public enum SendMsgCodeEnum {

    SUCCESS(0);

    public final Integer code;

    SendMsgCodeEnum(Integer code) {
        this.code = code;
    }
}
