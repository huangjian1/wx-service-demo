package com.pers.wxservice.service;

import cn.hutool.core.lang.Assert;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.pers.wxservice.enums.SendMsgCodeEnum;
import com.pers.wxservice.model.SendTextPojo;
import com.pers.wxservice.model.TextPojo;
import com.pers.wxservice.utils.WeChatUtil;
import com.pers.wxservice.value.WeChatConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 23:00
 */
@Service("weChatService")
@Slf4j
public class WeChatService {

    public void sendMessage(HttpServletRequest request) throws IOException {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = streamReader.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }
        //接收的信息
        JSONObject receiveJsonObject = JSONUtil.parseObj(responseStrBuilder.toString());
        //accessToken
        JSONObject accessTokenJsonObject = WeChatUtil.getAccessToken(WeChatConstant.appId, WeChatConstant.secret);

        Assert.notEmpty(receiveJsonObject.get("ToUserName").toString());
        Assert.notEmpty(accessTokenJsonObject.get("access_token").toString());

        log.info("receiveJsonObject === {}", receiveJsonObject);
        log.info("accessTokenJsonObject === {}", accessTokenJsonObject);

        String accessToken = accessTokenJsonObject.get("access_token").toString();

        String msgType = receiveJsonObject.get("MsgType").toString();
        if ("text".equals(msgType)) {
            SendTextPojo sendTextPojo = new SendTextPojo(receiveJsonObject.get("FromUserName").toString(), "text", new TextPojo("臭豆腐、腐乳、老干妈！"));
            String sendTextPojoJsonStr = JSONUtil.toJsonStr(sendTextPojo);
            String body = HttpRequest.post(MessageFormat.format(WeChatConstant.POST_SEND_MSG_URL, accessToken)).body(sendTextPojoJsonStr).execute().body();
            JSONObject sendMsgResult = JSONUtil.parseObj(body);

            if (sendMsgResult.get("errcode") != SendMsgCodeEnum.SUCCESS.code) {
                throw new RuntimeException("errorMsg === " + sendMsgResult.get("errmsg").toString());
            }
            log.info("sendMsg success");
        }
    }

}
