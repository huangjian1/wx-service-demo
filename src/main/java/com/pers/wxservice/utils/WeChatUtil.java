package com.pers.wxservice.utils;

import cn.hutool.http.HttpConnection;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.pers.wxservice.value.WeChatConstant;

import java.text.MessageFormat;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 23:08
 */
public class WeChatUtil {

    public static JSONObject getAccessToken(String appId,String secret){
        String getAccessToken = MessageFormat.format(WeChatConstant.GET_ACCESS_TOKEN_URL,appId,secret);
        String post = HttpRequest.get(getAccessToken).execute().body();
        JSONObject accessTokenJSONObject = JSONUtil.parseObj(post);
        return accessTokenJSONObject;
    }

}
