package com.pers.wxservice.utils;

import cn.hutool.core.lang.Assert;

/**
 * @Author: 黄建
 * @Date: 2020/4/11 22:18
 */
public class SignatureUtil {

    public static Boolean checkSignature(String signature,String sha1){
        Assert.notEmpty(sha1);
        Assert.notEmpty(signature);
        if (sha1.equals(signature)){
            return true;
        }
        return false;
    }
}
